﻿//====================================================================
// OnHover for in game buttons Class
// Description:		changes the sprite used for buttons if the onhighlight function is called
// Created By: 		Ryan Simpson
// Date: 			06/04/14
// Edits:			
// Acknowledgements:
//====================================================================


using UnityEngine;
using System.Collections;

public class OnHover : MonoBehaviour 
{
	//sprites for if the button is hovered over or not
	public Sprite mNormal;													
	public Sprite mHighlighted;							

	//renderer used to switch sprites
	private SpriteRenderer mSpriteRenderer;				

	//if the button should be highlighted
	private bool mHighlight;							


	// Use this for initialization
	void Start () 
	{
		mHighlight = false;
		mSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// change the sprite depending on if its hovered over or not
	void Update () 
	{
		if(mHighlight)
		{
			mSpriteRenderer.sprite = mHighlighted;
		}
		else
		{
			mSpriteRenderer.sprite = mNormal;
		}
		mHighlight = false;
	}

	//called if the mouse is over the button
	void OnHighlight()
	{
		mHighlight = true;
	}
}
