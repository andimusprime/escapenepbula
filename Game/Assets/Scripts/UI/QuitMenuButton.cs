﻿//====================================================================
// Quit Button Class
// Description:	On click will quit application - used for menus	
// Created By: Ryan Simpson
// Date: 25/03/14
// Edits:
// Acknowledgements:
//==================================================================== 

using UnityEngine;
using System.Collections;

public class QuitMenuButton : MonoBehaviour {

	// Use this for initialization
	//repositions the button to be placed relative to screen size
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//on click return to menu
	void OnClick()
	{
		Application.LoadLevel("MainMenu");
	}
}
