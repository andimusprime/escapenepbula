﻿//====================================================================
// Retry Button in pause menu code
// Description:		will go to next level on click
// Created By: 		Ryan Simpson
// Date: 			31/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class NextLvlMenuButton : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//on click find out what level the player is on and load the next level
	void OnClick()
	{
		switch (Managers.Game.GetLevel ()) 
		{
			case GameManager.Level.ONE:
				Managers.Game.SetLevel(GameManager.Level.TWO);
				Application.LoadLevel("SRLevel2");
				break;
			case GameManager.Level.TWO:
				Managers.Game.SetLevel(GameManager.Level.THREE);
				Application.LoadLevel("SRLevel3");
				break;
			case GameManager.Level.THREE:
				Managers.Game.SetLevel(GameManager.Level.ONE);
				Application.LoadLevel("MainMenu");
				break;
			default:
				Managers.Game.SetLevel(GameManager.Level.ONE);
				Application.LoadLevel("MainMenu");
				break;
		}
	}
}
