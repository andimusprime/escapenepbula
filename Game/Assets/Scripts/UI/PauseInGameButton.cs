﻿//====================================================================
// Pause button class
// Description:		on click will pause the game
// Created By: 		Ryan Simpson
// Date: 			21/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class PauseInGameButton : MonoBehaviour 
{

	public GameObject mPauseMenu;

	// Use this for initialization
	//repositions using screen dimensions
	void Start () 
	{
		Rescale ();
	}

	//positions the pause button at the side of the screen
	//scale it to be 4% of the width and height of the screen
	void Rescale()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint( new Vector3( Camera.main.pixelWidth * 0.98f, Camera.main.pixelHeight/10, 9 ) );
		//change the position
		pos.z = transform.position.z;
		pos.y = transform.position.y;
		transform.position = pos;
		
		Vector3 scale = transform.localScale;
		
		scale.x /= 53;
		scale.y /= 29;
		
		scale.x *= Camera.main.pixelWidth * 0.04f;
		scale.y *= Camera.main.pixelHeight * 0.04f;
		
		transform.localScale = scale;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//if the object has been clicked this is called
	void OnClick()
	{
		Managers.Game.SwitchPaused ();			//change the pause state
		if(Managers.Game.GetPaused())			//if the game is paused get the camera position and create an instance of the pause menu
		{		
			Vector3 pos = Managers.Game.GetCameraPosition();
			pos.z = -2;
			GameObject clone = (GameObject)Instantiate(mPauseMenu,pos,Quaternion.identity );
		}
	}
}
