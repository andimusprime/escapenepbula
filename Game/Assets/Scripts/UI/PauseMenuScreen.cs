﻿//====================================================================
// Pause Menu Class
// Description:		Controls what is displayed when paused
// Created By: 		Ryan Simpson
// Date: 			20/03/14
// Edits:			Added Rescale Method - Ryan Simpson
//					08/04/14 Andrew Allan: Removed scaling method due to build issues
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class PauseMenuScreen : MonoBehaviour 
{

	public Sprite mPaused;			//different sprites for different pause states
	public Sprite mHelp;

	private SpriteRenderer mSpriteRenderer;		//renderer used to switch sprites

	
	// Use this for initialization
	void Start () 
	{
		mSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		Rescale();
	}
	
	//Position the screen in the centre of the screen
	//scale it to the height and width of the screen
	private void Rescale()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint( new Vector3( Camera.main.pixelWidth/2, 0, 5 ) );
		//change the position
		pos.z = transform.position.z;
		pos.y = transform.position.y;
		transform.position = pos;
		
		Vector3 scale = transform.localScale;
		
		//scale.x /= 960;
		//scale.y /= 544;
		
		//scale.x *= Camera.main.pixelWidth;
		//scale.y *= Camera.main.pixelHeight;
		
		transform.localScale = scale;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//if the game is no longer paused delete the pause screen
		if(!Managers.Game.GetPaused())
		{
			Destroy (this.gameObject);
		}

		//change the sprite to be the help splash screen
		if(Managers.Game.GetHelpDisplayed())
		{
			mSpriteRenderer.sprite = mHelp;
		}
		else
		{
			mSpriteRenderer.sprite = mPaused;
		}
	}
}
