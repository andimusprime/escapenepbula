﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour 
{
	public float mScrollSpeed = 0.0f;

	// Update is called once per frame
	void Update () 
	{
		renderer.material.mainTextureOffset = new Vector2((Time.time * mScrollSpeed)%1, 0.0f);
	}
}
