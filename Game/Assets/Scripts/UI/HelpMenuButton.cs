﻿//====================================================================
// Help Button in pause menu code
// Description:		On click changes if the help options are displayed
// Created By: 		Ryan Simpson
// Date: 			31/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class HelpMenuButton : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	//on click change the screen displayed in the pause menu
	void OnClick()
	{
		Managers.Game.SwitchHelpDisplayed();
	}
}
