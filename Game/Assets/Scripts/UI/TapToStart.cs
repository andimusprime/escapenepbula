﻿//====================================================================
// Start Screen Class
// Description:		On click it will unpause the game and destroy itself to start the game
// Created By: 		Ryan Simpson
// Date: 			02/04/14
// Edits:			08/04/14 Andrew Allan: Removed scaling method due to build issues
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class TapToStart : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		Rescale();
	}

	//Position the screen in the centre of the screen
	//scale it to the height and width of the screen
	void Rescale()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint( new Vector3( Camera.main.pixelWidth/2, 0, 5 ) );
		//change the position
		pos.z = transform.position.z;
		pos.y = transform.position.y;
		transform.position = pos;

		Vector3 scale = transform.localScale;

		//scale.x /= 960;
		//scale.y /= 544;

		//scale.x *= Camera.main.pixelWidth;
		//scale.y *= Camera.main.pixelHeight;

		transform.localScale = scale;

	}


	// Update is called once per frame
	void Update () 
	{

	}

	//unpause and destroy itself
	void OnClick()
	{
		Managers.Game.SwitchPaused();
		Destroy (this.gameObject);
	}
}