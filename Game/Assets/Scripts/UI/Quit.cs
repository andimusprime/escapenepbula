﻿//====================================================================
// Quit Button Class
// Description:	On click will quit application - used for menus	
// Created By: Ryan Simpson
// Date: 25/03/14
// Edits:
// Acknowledgements:
//==================================================================== 

using UnityEngine;
using System.Collections;

public class Quit : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//close the application
	void OnClick()
	{
		Application.Quit();
	}
}
