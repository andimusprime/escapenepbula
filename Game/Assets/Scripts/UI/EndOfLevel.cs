﻿//====================================================================
// End of Level 
// Description:		Applies to splash screen,  on click loads the next level
// Created By: 		Ryan Simpson
// Date: 			20/03/14
// Edits:			Added Rescale Method - Ryan Simpson
//					08/04/14 Andrew Allan: Removed scaling method due to build issues
// Acknowledgements:
//====================================================================


using UnityEngine;
using System.Collections;

public class EndOfLevel : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		Rescale ();
	}

	//Position the screen in the centre of the screen
	//scale it to the height and width of the screen
	private void Rescale()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint( new Vector3( Camera.main.pixelWidth/2, 0, 5 ) );
		//change the position
		pos.z = transform.position.z;
		pos.y = transform.position.y;
		transform.position = pos;
		
		Vector3 scale = transform.localScale;
		
		//scale.x /= 960;
		//scale.y /= 545;
		
		//scale.x *= Camera.main.pixelWidth;
		//scale.y *= Camera.main.pixelHeight;
		
		transform.localScale = scale;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//On click find the next level from the game manager and load it
	void OnClick()
	{
		//load the next level
		switch (Managers.Game.GetLevel ()) 
		{
		case GameManager.Level.ONE:
			Managers.Game.SetLevel(GameManager.Level.TWO);
			Application.LoadLevel("SRLevel2");
			break;
		case GameManager.Level.TWO:
			Managers.Game.SetLevel(GameManager.Level.THREE);
			Application.LoadLevel("SRLevel3");
			break;
		case GameManager.Level.THREE:
			Managers.Game.SetLevel(GameManager.Level.ONE);
			Application.LoadLevel("MainMenu");
			break;
		default:
			Managers.Game.SetLevel(GameManager.Level.ONE);
			Application.LoadLevel("MainMenu");
			break;
		}
	}
}
