﻿//====================================================================
// Retry Button in pause menu code
// Description:		On click restarts the level
// Created By: 		Ryan Simpson
// Date: 			31/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class RetryMenuButton : MonoBehaviour {

	// Use this for initialization
	//repositions the button to be placed relative to screen size
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//on click re load the level
	void OnClick()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
}
