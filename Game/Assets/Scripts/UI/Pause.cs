﻿//====================================================================
// Pause button class
// Description:		on click will pause the game
// Created By: 		Ryan Simpson
// Date: 			21/10/13
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	public GameObject mPauseMenu;
	private GameObject mDisplay;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
	}

	//Switch the state of the game in the game manager
	void OnClick()
	{
		Managers.Game.SwitchPaused ();
		DisplayPauseScreen();
	}
	
	//creates an instance of the pause menu at the camera position
	public void DisplayPauseScreen()
	{
		if( mPauseMenu != null )
		{
			if( mDisplay == null )
			{
				Vector3 pos = Managers.Game.GetCameraPosition();
				pos.z = -2;
				mDisplay = (GameObject)Instantiate(mPauseMenu,pos,Quaternion.identity );
			}
		}
	}
}
