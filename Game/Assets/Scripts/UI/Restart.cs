﻿//====================================================================
// Restart button class
// Description:		on click will restart the level
// Created By: 		Ryan Simpson
// Date: 			21/10/13
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class Restart : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//return theplayer to the start
	void OnClick()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
}
