﻿//====================================================================
// Restart button class
// Description:		on click will restart the level
// Created By: 		Ryan Simpson
// Date: 			21/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class RestartInGameButton : MonoBehaviour 
{

	// Use this for initialization
	//resizes and repositions using screen dimensions
	void Start () 
	{
		Rescale ();
	}
	
	//positions object at the side of the screen
	//scales it to 4% of the screens height and width
	private void Rescale()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint( new Vector3( Camera.main.pixelWidth/65, Camera.main.pixelHeight/10, 9 ) );
		//change the position
		pos.z = transform.position.z;
		pos.y = transform.position.y;
		transform.position = pos;
		
		Vector3 scale = transform.localScale;
		
		scale.x /= 49;
		scale.y /= 29;
		
		scale.x *= Camera.main.pixelWidth * 0.04f;
		scale.y *= Camera.main.pixelHeight * 0.04f;
		
		transform.localScale = scale;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//return theplayer to the start
	void OnClick()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
}
