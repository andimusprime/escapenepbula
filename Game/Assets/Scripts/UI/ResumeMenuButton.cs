﻿//====================================================================
// Resume button class
// Description:		Functionality for the resume button in the pause button
// Created By: 		Ryan Simpson
// Date: 			21/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class ResumeMenuButton : MonoBehaviour {

	// Use this for initialization
	//repositions the button to be placed relative to screen size
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	//if the object has been clicked this is called
	void OnClick()
	{
		Managers.Game.SwitchPaused ();			//change the pause state
	}
}
