﻿//====================================================================
// Fall Test
// Description:		Checks the player has not fallen off screen
// Created By: 		Andrew Allan
// Date: 			27/03/14 
// Edits:			
// To do: 			Does not work, fix?
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class FallTest : MonoBehaviour {

	private Vector2 mStartPoint;
	public Vector2 mEndPoint;

	// Use this for initialization
	void Start () 
	{
		mStartPoint = new  Vector2( transform.position.x, transform.position.y );
	}
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit2D hit = Physics2D.Linecast( mStartPoint, mEndPoint, 1<< 8, -1.0f, 1.0f );
		if ( hit.collider != null )
		{
			Player player = GetComponent<Player>();
			Vector3 position = new  Vector3( -5.0f, 2.0f, 0.0f );
			player.transform.position = position;
		}
	}
}
