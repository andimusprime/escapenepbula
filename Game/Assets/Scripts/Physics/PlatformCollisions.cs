﻿//====================================================================
// Collision Class
// Description:		Detects and reacts to platform collisions
// Created By: 		Andrew Allan
// Date: 			18/10/13
// Edits:			27/02/14 Andrew Allan: Altered to work with unity2D 
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

[RequireComponent( typeof ( BoxCollider2D ) ) ]

public class PlatformCollisions : MonoBehaviour 
{
	[SerializeField] private int mNumberOfRays = 3;
	[SerializeField] private LayerMask mLayerMask = 0;
	private BoxCollider2D mCollider;
	private Vector3 mCentre;
	private Vector3 mSize;
	private const float mSPACING = 0.0001f; //stop player overlapping the platfroms
	
	
	// Use this for initialization
	void Start () 
	{
		//Gets the attached collider along and store size
		mCollider = GetComponent <BoxCollider2D> ();
		mCentre = mCollider.center;
		mSize = mCollider.size;
	}
	
	public Vector2 CollisionDetection( Vector2 speed, ref bool isGrounded, ref bool wallHit ) 
	{//Detects collisions with platfroms
		
		if ( speed.y != 0 ) {
			VerticalCollision( ref speed, ref isGrounded );
		}
		if ( speed.x != 0 ) {
			HorizontalCollision( ref speed, ref wallHit );
		}

		DirectionCollision ( ref speed, ref isGrounded, ref wallHit ); 

		return speed;
		
	}
	
	private void VerticalCollision( ref Vector2 speed, ref bool isGrounded ) 
	{//Checks for collisons in the vertical axis
		
		Vector2 position = transform.position; //local position
		RaycastHit2D hit;
		
		for ( int rays = 0; rays <= mNumberOfRays; rays++ ) 
		{	
			Vector2 origin;
			float direction = Mathf.Sign( speed.y );	//Get the sign of the vertical speed
			//casts rays from the bottom left, centre and right of the collider
			origin.x = ( position.x + mCentre.x -  mSize.x / 2  ) + ( ( mSize.x / mNumberOfRays ) * rays  );
			origin.y = position.y + mCentre.y + ( mSPACING * direction ) + mSize.y / 2  * direction;

			//checks for the ray hitting a collider	
			hit = Physics2D.Raycast( origin, new Vector2 ( 0, direction ),  Mathf.Abs( speed.y ), mLayerMask );
			if( hit.collider != null ){
				//Get distance between player and ground
				float distance = Vector2.Distance( origin, hit.point );
				
				//Stop players downward movement
				if ( distance > mSPACING ) {
					speed.y = distance * direction + mSPACING;
				}
				else {
					speed.y = 0;
					
				}
				isGrounded = true;
				break;
			}
			else {
				Debug.DrawRay( new Vector3( origin.x, origin.y, 0.0f ),  new Vector3( 0.0f, direction, 0.0f ) );
				isGrounded = false;
			}
		}
	}
	
	private void HorizontalCollision( ref Vector2 speed, ref bool wallHit ) 
	{//Checks for collisons in the vertical axis
		
		Vector3 position = transform.position; //local position
		RaycastHit2D hit;
		
		for ( int rays = 0; rays <= mNumberOfRays; rays++ ) 
		{	//casts rays from the bottom left, centre and right of the collider
			Vector3 origin;
			float direction = Mathf.Sign( speed.x ); //Get the sign of the vertical speed
			//casts rays from the side top, centre and bottom of the collider
			origin.x = position.x + mCentre.x + ( mSPACING * direction ) + mSize.x / 2  * direction;
			origin.y = ( position.y + mCentre.y -  mSize.y / 2  ) + ( ( mSize.y / mNumberOfRays ) * rays ) ;
			origin.z = position.z;
			//cast a ray in the direction of the vertical speed
			hit = Physics2D.Raycast( origin, new Vector2 ( direction, 0 ),  Mathf.Abs( speed.x ), mLayerMask );

			if( hit.collider != null ){

				//Get distance between player and a wall
				float distance = Vector2.Distance( origin, hit.point );
				
				//Stop players horizontal movement
				if ( distance > mSPACING ) {
					speed.x = distance * direction;
					if ( direction > 0 ) {
						speed.x -= mSPACING;
					}
					else {
						speed.x += mSPACING;
					}
				}
				else {
					speed.x = 0;
				}
				wallHit = true;
				break;
				
			}
			else {
				wallHit = false;	
				Debug.DrawRay( new Vector3( origin.x, origin.y, 0.0f ),  new Vector3( direction, 0.0f, 0.0f ) );
			}
		}
	}
	private void DirectionCollision( ref Vector2 speed, ref bool isGrounded,  ref bool wallHit ) 
	{//Checks for collisons in the direction of movement 
	
		if (!isGrounded && !wallHit) {
			Vector3 position = transform.position; //local position
			Vector3 playerDirection = speed;
			RaycastHit2D hit;

			playerDirection.Normalize();
			Vector3 origin = new Vector2( position.x + mCentre.x + mSize.x/2 * Mathf.Sign( speed.x ),position.y + mCentre.y + mSize.y/2 * Mathf.Sign( speed.y ));
		
			//cast a ray in the direction of the speed
			hit = Physics2D.Raycast( origin, playerDirection, Mathf.Sqrt( speed.x * speed.x + speed.y * speed.y ), mLayerMask );
		
			if( hit.collider != null ){
				isGrounded = true;
				wallHit = true;
				speed.x = 0;
				speed.y = 0;
			}

		}
	}
}
