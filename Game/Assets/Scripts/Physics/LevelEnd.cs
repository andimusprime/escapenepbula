﻿//====================================================================
// Level End class
// Description:		Checks if the player has reached the end of the level
// Created By: 		
// Date: 			
// Edits:			Now creates a level Complete game object - Ryan Simpson
// Acknowledgements:
//====================================================================


using UnityEngine;
using System.Collections;

public class LevelEnd : MonoBehaviour 
{
	public GameObject mLevelEndScreen;

	private static float mTime = 3.0f;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!Managers.Game.GetPaused())
		{
		//if end of level pause game, show level complete screen, load next level after a time
			Vector2 position = new  Vector2( transform.position.x, transform.position.y );
			if ( Physics2D.OverlapCircle( position , 0.1f, 1 << 8, -1.0f, 1.0f ) != null )
			{
				Managers.Game.SetPaused ( true );

				//create level complete screen
				Vector3 pos = Managers.Game.GetCameraPosition();
				pos.z = -2;
				GameObject clone = (GameObject)Instantiate(mLevelEndScreen, pos,Quaternion.identity );

			}
		}
	}
	
}
	
	
