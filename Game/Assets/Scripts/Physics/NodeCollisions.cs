﻿//====================================================================
// Node Collision Class
// Description:		Detects when an object hits a Wall Node
// Created By: 		Greig Barker
// Date: 			04/03/14
// Edits:			05/03/14 Greig Barker: Got nodes to apply speed to the wall
//					10/03/14 Greig Barker: Cleaned up code and added comments where necessary
// Acknowledgements: Andrew Allan: Ray casting for detecting wall nodes
//					 Unity Website: http://docs.unity3d.com/Documentation/ScriptReference/GameObject.FindGameObjectsWithTag.html
//====================================================================

using UnityEngine;
using System.Collections;

[RequireComponent( typeof ( BoxCollider2D ) ) ]
[RequireComponent( typeof ( WallNode ) ) ]

public class NodeCollisions : MonoBehaviour 
{
	[SerializeField] private int mNumberOfRays = 4;
	[SerializeField] private LayerMask mLayerMask = 0;
	private BoxCollider2D mCollider;
	private WallNode mWallNode;
	private Vector3 mCentre;
	private Vector3 mSize;
	private const float mSPACING = 0.0001f; //stop player overlapping the platfroms
	GameObject[] WallNodes;	//Array which keeps track of all the wall nodes on the level
	GameObject ClosestWallNode;	//GameObject returned by FindClosestWallNode() function
	
	// Use this for initialization
	void Start () 
	{
		//Gets the attached collider along and store size
		mCollider = GetComponent <BoxCollider2D> ();
		mWallNode = GetComponent <WallNode> ();
		mCentre = mCollider.center;
		mSize = mCollider.size;	
		WallNodes = GameObject.FindGameObjectsWithTag("WallNode"); //Fills the array with all Wall Nodes on the level
	}

	GameObject FindClosestWallNode() 
	{
		WallNodes = GameObject.FindGameObjectsWithTag("WallNode");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject gameObject in WallNodes) 
		{
			//Check the difference between the WallNodes position and the DeathWalls position
			Vector3 diff = gameObject.transform.position - position; 
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) 
			{
				ClosestWallNode = gameObject;
				distance = curDistance;
			}
		}

		return ClosestWallNode;
	}
	
	public Vector2 CollisionDetection(Vector2 _speed) 
	{//Detects collisions with the wall nodes
		
		CheckNodeHit( ref _speed);	
		return _speed;
	}

	private void CheckNodeHit( ref Vector2 _speed) 
	{//Checks for collisons in the vertical axis
		
		Vector3 position = transform.position; //local position
		RaycastHit2D hit;
		
		for ( int rays = 0; rays <= mNumberOfRays; rays++ ) 
		{	
			//casts rays from the bottom left, centre and right of the collider
			Vector3 origin;
			float direction = Mathf.Sign( _speed.x ); //Get the sign of the vertical speed
			//casts rays from the side top, centre and bottom of the collider
			origin.x = position.x + mCentre.x + ( mSPACING * direction ) + mSize.x / 2  * direction;
			origin.y = ( position.y + mCentre.y -  mSize.y / 2  ) + ( ( mSize.y / mNumberOfRays ) * rays ) ;
			origin.z = position.z;
			//cast a ray in the direction of the horizontal speed
			hit = Physics2D.Raycast( origin, new Vector2 ( direction, 0 ),  Mathf.Abs( _speed.x ), mLayerMask );

			//Check if the raycast has hit a WallNode
			if( hit.collider != null )
			{
				//Set the death walls internal WallNode script to be the same as the closest
				//wallNode which was returned by the function.
				mWallNode = (WallNode)FindClosestWallNode().gameObject.GetComponent("WallNode"); 
				//Set DeathWalls speed to the Target speed contained in the wallNode
				if(mWallNode.mTargetWallSpeed != _speed.x)
				{
					_speed.x = mWallNode.mTargetWallSpeed;
					Debug.Log("Hit Wall Node");
				}
				break;				
			}
			else{
				Debug.DrawRay( new Vector3( origin.x, origin.y, 0.0f ),  new Vector3( direction, 0.0f, 0.0f ) );
			}
		}
	}
	
}
