﻿//====================================================================
// Words Script
// Description:		applies changes over time to kinematic typography in the game
// Created By: 		Ryan Simpson
// Date: 			10/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class WordsScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//increase transparency of the object
		Color textureColor = renderer.material.color;
		textureColor.a = textureColor.a - 0.025f;
		renderer.material.color = textureColor;

		//make the object float up and to the right
		Vector3 pos = transform.position;
		pos.x = pos.x - 0.01f;
		pos.y = pos.y + 0.01f;
		transform.position = pos;

		//make the object slightly larger
		Vector3 scale = transform.localScale;
		scale.x += 0.01f;
		scale.y += 0.01f;
		transform.localScale = scale;

	}
}
