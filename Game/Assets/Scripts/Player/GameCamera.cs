﻿//====================================================================
// Game Manager Class
// Description:		Gives camera it's position to follow the gameplay
// Created By: 		Dziek Dyes-Bolt
// Date: 			10/03/14
// Edits:			Added check on click method, allowing interaction - Ryan Simpson
//					Added Check on hover method, checks if the mouse is over a button
//					while the game is paused	-	Ryan Simpson
//					Removed camera judder when you fall of screen - Dziek Dyes-Bolt
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;


public class GameCamera : MonoBehaviour {

	//public Transform player;
	private bool mCameraLock = true;


	// Use this for initialization
	void Start () 
	{
	}

	// Update is called once per frame
	void Update () 
	{

		//send the cameras position to the game manager
		Managers.Game.SetCameraPosition(transform.position);

		if ((mCameraLock) && (Input.GetKeyDown("u")))
		{
			mCameraLock = false;
		}
		
		if ((mCameraLock == false) && (Input.GetKeyDown("l")))
		{
			mCameraLock = true;
		}

		if (Input.GetMouseButtonDown (0)) 
		{
			//check with raycast if mouse click is on something
			CheckOnClick ();
		}

		if(Managers.Game.GetPaused())
		{
			CheckOnHover ();
		}
		
	}

	//if the game is paused this will check if any buttons are being hovered over and will call their onhightlight function
	void CheckOnHover()
	{
		//get the position of the click in game
		Vector3 clickPosition = camera.ScreenToWorldPoint (Input.mousePosition);
		
		//raycast is just a dot
		RaycastHit2D hit = Physics2D.Linecast (clickPosition, clickPosition);
		
		if (hit) 
		{
			//get the game object from the collider and send a message to run a function
			GameObject clickedObject = hit.collider.gameObject;
			clickedObject.SendMessage ("OnHighlight", SendMessageOptions.DontRequireReceiver);
		}
	}

	//checks if anything with a box collider and on click function has been clicked
	void CheckOnClick()
	{
		//get the position of the click in game
		Vector3 clickPosition = camera.ScreenToWorldPoint (Input.mousePosition);

		//raycast is just a dot
		RaycastHit2D hit = Physics2D.Linecast (clickPosition, clickPosition);

		if (hit) 
		{
			//get the game object from the collider and send a message to run a function
			GameObject clickedObject = hit.collider.gameObject;
			clickedObject.SendMessage ("OnClick", SendMessageOptions.DontRequireReceiver);
		}
	}

	void Restarted()
	{

	}
	
	void LateUpdate() {
		
		//Moved to lateUpdate to remove judder on death
		if (mCameraLock)
		{
			transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
		}
	
	}
}
