﻿//====================================================================
// Next Level Class
// Description:		Sets the level for the player in the game manager
// Created By: 		Ryan Simpson
// Date: 			20/03/14
// Edits:			
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour 
{
	//allows the level to be set for each scene
	public GameManager.Level mThisLvl;

	// Use this for initialization
	void Start () 
	{
		Managers.Game.SetLevel(mThisLvl);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
