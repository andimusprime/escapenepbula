﻿//====================================================================
// Player Class
// Description:		Stores player related variable and handle the update each frame
// Created By: Andrew Allan
// Date: 18/10/13
// Edits:	Converted to use Unity's 2D functionality
//			Added Player sound effects (AudioClips) - played while walking or when jumping - Ryan Simpson
//			Added counter - after X frames of movement it will instantiate a 'comic word'
//			game object and destroy it again after 2 seconds							   - Ryan Simpson
//			Checks to see if the game is paused before any updates are called - Ryan Simpson
//			Added animations for idle, running, jumping and death - Dziek Dyes-Bolt
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

//Adds required functions
[RequireComponent( typeof ( PlatformCollisions ) ) ]

//Requires Audio
[ RequireComponent( typeof ( AudioSource ) ) ]

public class Player : MonoBehaviour 
{
	private Animator mAnimator;

	private PlatformCollisions mCollisions;
	public Vector2 mAcceleration;
	private float mCurrentHorizontalAcceration;
	private float mTargetHorizontalSpeed;
	private Vector2 mSpeed;
	public Vector2 mMaxSpeed;
	private Vector2 mMoveAmount;
	private bool mIsGrounded = false;
	private bool mPrevGrounded = false;
	private bool mJumpPushed = false;
	private bool mJumpReleased = false;
	private bool mIsJumping = false;
	private bool mWallHit = false;
	private bool mIsDead = false;
	private float mJumpTime;
	private float mDeathJumpHeight = 12;
	public float mMaxJumpTime;
	private Vector3 mStartPosition;

	public AudioClip mJump;				//jump audio clip
	public AudioClip mSteps;			//audio clip for footsteps
	private bool mStepsPlaying = false;	//boolean telling if steps is playing

	public GameObject[] mStepWords;	    //clang word to appear when walking
	private int mStepsCount = 0;		//number of steps walked - used to tell when to display another word
	private static int mMakeWord = 120;	//number the counter must go up to before making another word
	
	// Use this for initialization
	void Start () 
	{
		//Creates local reference to attached scripts
		mCollisions = GetComponent <PlatformCollisions>();
		
		//Creates local reference to attached animator
		mAnimator = this.GetComponent<Animator>();
		
		mStartPosition = transform.position;
	
	}
	
	// Update is called once per frame
	void Update () 
	{	
		//if games paused dont do update things	-	Ryan Simpson
		if(!Managers.Game.GetPaused())
		{	
			//Move the player
			Movement();
			
			//if the player is not dead check for collisons
			if (mIsDead == false)
			{
				mMoveAmount = mCollisions.CollisionDetection( mMoveAmount, ref mIsGrounded, ref mWallHit );
			}
			//if the player is on the ground
			if ( mIsGrounded )
			{
				if(!mPrevGrounded)
				{
					DisplayWords ( 4 );			//show a prefab (4th word) for landing on the ground
				}
				mSpeed.y = 0;
				mIsJumping = false;
				
				mAnimator.SetBool("Jumping", false);
			}
			if ( mWallHit )
			{
				mSpeed.x = 0;			
			}

			mPrevGrounded = mIsGrounded;

			//if the player is moving, the sound is not playing, and not jumping
			//play the sound
			//else if the player is not moving and the sound is playing
			//stop the sound	-	Ryan Simpson
			if( ( mSpeed.x != 0 ) && ( !mStepsPlaying ) && ( mIsGrounded ) )
			{
				audio.PlayOneShot(mSteps);
				mStepsPlaying = true;
				
				mAnimator.SetBool("Running", true);
			}			
			else if ( ( mSpeed.x == 0 ) && ( mStepsPlaying ) )
			{
				audio.Stop ();
				mStepsPlaying = false;
				
				mAnimator.SetBool("Running", false);
			}
			
			if (( mSpeed.x == 0 ) && ( mIsGrounded ))
			{
				mAnimator.SetBool("Idle", true);
			}
			CheckHeight();
			Render();
			
		}
		else
		{
			//the game is paused so stop playing player sound effects
			audio.Stop ();
			mStepsPlaying = false;
		}
	}

	//create a new word to appear just below the player, destroy it after a delay and reset the distance travelled horizontally - Ryan Simpson
	private void DisplayWords(int word)
	{
		float y = transform.position.y - 0.5f;
		float x = transform.position.x - 1.0f;
		GameObject clone = (GameObject)Instantiate(mStepWords[word],new Vector3( x, y, -1 ),Quaternion.identity );
		Destroy (clone, 2);
		mStepsCount = 0;
	}
	
	//Handles the players movements, include jumping
	private void Movement ()
	{
	
		mTargetHorizontalSpeed = mMaxSpeed.x;

		if (mIsDead == false)
		{
			Accelerate();
		}
		CheckJumpPushed();

		if(mSpeed.x != 0 && mIsGrounded)	//while moving along the ground check if we want to display a 'comic style' word
		{
			mStepsCount++;
			if(mStepsCount > 80)		//after this many frames of horizontal movement 
			{
				DisplayWords ( Random.Range(0,4) );		//display a random word
			}
		}
		

		if ( mJumpPushed && mIsGrounded )
		{
			mIsJumping = true;
			mJumpTime = Time.time;
			mStepsPlaying = false;
			audio.Stop ();				//stop sound - stops step sounds in air
			audio.PlayOneShot(mJump);	//play the jump sound
			
			mAnimator.SetBool("Jumping", true);	//play jump animation
		}
		if ( mJumpReleased || (Time.time - mJumpTime > mMaxJumpTime ) ) 
		{
			mIsJumping = false;
		}
		if ( mIsJumping ) 
		{
			mSpeed.y += mAcceleration.y * Time.deltaTime;
		}
		
		Managers.PhysicsMan.ApplyGravity( ref mSpeed.y );
		mMoveAmount = mSpeed * Time.deltaTime; //adjusts movement so its relative to frames per second
	}
	
	private void Render ()
	{
		transform.Translate(new Vector3( mMoveAmount.x, mMoveAmount.y, 0 ) );
	}
	
	//Accelerate the player towards target speed
	private void Accelerate()
	{	
		if ( mSpeed.x != mTargetHorizontalSpeed ) 
		{
			float direction = Mathf.Sign( mTargetHorizontalSpeed - mSpeed.x );	
			mSpeed.x += (mAcceleration.x * direction * Time.deltaTime);

			if ( direction != Mathf.Sign( mTargetHorizontalSpeed - mSpeed.x ))
			{
				mSpeed.x = mTargetHorizontalSpeed;
			}
		}
	}
	
	//Check if the jump button is pushed depandant on the platform 
	private void CheckJumpPushed(){
	
		mJumpPushed = false;
		mJumpReleased = false;
	
	#if UNITY_EDITOR || UNITY_STANDALONE_WIN
	
		if ( Input.GetButtonDown( "Jump" ) ){
			mJumpPushed = true;
		}
		if ( Input.GetButtonUp( "Jump" ) ){
			mJumpReleased = true;
		}
	#else
	
		if( GetComponentInChildren<ScreenHit>().GetIsHit() ){
			mJumpPushed = true;
		}
		if( GetComponentInChildren<ScreenHit>().GetIsReleased() ) {
			mJumpReleased = true;
		}
	#endif
	}
	
	//Check the player is still on the screen. If not begin the death animation
	private void CheckHeight(){
		
		if ( transform.position.y <= -3.0f )
		{
			if (mIsDead == false)
			{
				mSpeed.y = 0;
				mSpeed.y += mDeathJumpHeight;
				mIsDead = true;
				mAnimator.SetBool("DeathUp", true);
				mSpeed.x = 0;
			}

			if ((mIsDead == true) && (transform.position.y <= -5.0f))
			{
				Reset();
				Managers.Game.MakeTapSplashScreen();
				
			}

		}
	}
	
	private void Reset(){
	
		transform.position = mStartPosition;
		mSpeed.y = 0;
		mIsJumping = false;
		mIsDead = false;
		mAnimator.SetBool("DeathUp", false);
		mAnimator.SetBool("Running", false);
		mAnimator.SetBool("Jumping", false);
		mAnimator.SetBool("Idle", true);	
	}
}
