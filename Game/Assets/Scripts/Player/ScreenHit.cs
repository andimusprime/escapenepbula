﻿using UnityEngine;
using System.Collections;

public class ScreenHit: MonoBehaviour {

	private bool mIsHit;
	private bool mIsReleased;

	// Use this for initialization
	void Start () {
		mIsHit = false;
	}
	
	// Update is called once per frame
	void Update () {
		//check for touches
		mIsHit = false;
		mIsReleased = false;
		if( Input.touchCount > 0 ){
			Touch touch = Input.GetTouch(0);
			switch( touch.phase){
				
			case TouchPhase.Began:
				mIsHit = true;
				break;
			case TouchPhase.Moved:
				
				break;
			case TouchPhase.Ended:
				mIsReleased = true;
				break;
			}
		}
	}
	
	public bool GetIsHit(){
		return mIsHit;
	}
	
	public bool GetIsReleased(){
		return mIsReleased;
	}
	
}
