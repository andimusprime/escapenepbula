﻿//====================================================================
// WallNode Class
// Description:		Speeds the death wall up when it collides with one of the nodes
// Created By: 		Greig Barker
// Date: 			04/03/14
// Edits:
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

[RequireComponent( typeof ( BoxCollider2D ) ) ]

public class WallNode : MonoBehaviour
{
	
	public GameObject WallNodeObject;
	private BoxCollider2D mCollider;
	[SerializeField] public float mTargetWallSpeed = 0.0f;
	
	// Use this for initialization
	void Start () 
	{//Gets the attached collider along and store size		
		mCollider = GetComponent <BoxCollider2D> ();
	}
	
	float getWallTargetSpeed()
	{
		return mTargetWallSpeed * Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
}

