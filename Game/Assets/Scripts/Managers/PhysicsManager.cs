﻿//====================================================================
	// Physics Manager Class
	// Description:		Houses functions for the games simulation of physics
	// Created By: 		Andrew Allan
	// Date: 			18/10/13
	// Edits:			
	// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class PhysicsManager : MonoBehaviour {

	public float mAccleration;
	
	public float ApplyGravity ( ref float speed )
	{
		speed -= mAccleration * Time.deltaTime;
		return speed;
	}
}
