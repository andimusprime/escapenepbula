﻿//====================================================================
// Game Manager Class
// Description:		Controls game logic
// Created By: 		Andrew Allan
// Date: 			18/10/13
// Edits:			Added paused, help and camera position functions - Ryan Simpson
//					Added MakeTapSplashScreen function	-	Ryan Simpson
//					Added Enum and member which holds the level the player is currently on - Ryan Simpson
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	private bool mPaused = true;			//if the game is paused or not
	private bool mHelp = false;				//if the help/controls page should be displayed
	public GameObject mTapToStartSplash;	//object to be made for the tap to start splash screen

	private Vector3 mCameraPosition;		//Vector3 holding camera position
	private GameObject mSplash;				//splash screen instance

	public enum Level { ONE, TWO, THREE };	//enum of the levels the player could be on
	private Level mCurLevel;				//holds the level the player is currently on
	
	// Use this for initialization
	void Start ()
	{
		Debug.Log ("Start called");
		//mCurLevel = Level.ONE;
		MakeTapSplashScreen();
	}

	// Update is called once per frame
	void Update () 
	{
	
		if ((Input.GetKeyUp("r")) || (Input.GetButtonUp("Fire2"))) 
		{
			Application.LoadLevel(Application.loadedLevel);
		}

		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.Quit();
		}
	}

	//set what level the player is on
	public void SetLevel(Level _lvl)
	{
		mCurLevel = _lvl;
	}

	//return the level the player is on
	public Level GetLevel()
	{
		return mCurLevel;
	}

	//make the tap to start screen - should be called after the player has been repositioned
	public void MakeTapSplashScreen()
	{
		//set game to be paused and make a splash screen
		SetPaused(true);
		Vector3 pos = Camera.main.ScreenToWorldPoint( new Vector3( (Camera.main.pixelWidth/1.8f), Camera.main.pixelHeight/10, -2 ) );
		pos.y = mCameraPosition.y;
		pos.z = -7;

		mSplash = (GameObject)Instantiate(mTapToStartSplash, pos, Quaternion.identity);
	}
	
	//set the position the camera is at
	public void SetCameraPosition(Vector3 _position)
	{
		mCameraPosition = _position;
	}

	//return the position the camera is at
	public Vector3 GetCameraPosition()
	{
		return mCameraPosition;
	}

	//set if the game is paused to true/false
	public void SetPaused(bool _paused)
	{
		mPaused = _paused;
	}

	//return if the game is currently paused
	public bool GetPaused()
	{
		return mPaused;
	}

	//make paused the opposite of what it is now
	public void SwitchPaused()
	{
		mPaused = !mPaused;
	}


	//return if the help screen should be displayed
	public bool GetHelpDisplayed()
	{
		return mHelp;
	}
	
	//switch if the help screen is displayed
	public void SwitchHelpDisplayed()
	{
		mHelp = !mHelp;
	}
}
