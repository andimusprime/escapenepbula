﻿                                           //====================================================================
	// Manager Class
	// Description:		Creates static references to the games managers
	// Created By: 		Andrew Allan
	// Date: 			18/10/13
	// Edits:			21/03/14: Changed to use the singleton design pattern
	// Acknowledgements: 
//====================================================================

using UnityEngine;
using System.Collections;

[RequireComponent ( typeof ( PhysicsManager ) ) ]
[RequireComponent ( typeof ( GameManager ) ) ]
public class Managers : Singleton< Managers > {

	protected Managers(){}
	private static GameManager mGameManager;
	private static PhysicsManager mPhysicsManager;
	
	void Awake()
	{
		mGameManager = GetComponent< GameManager >( );
		mPhysicsManager = GetComponent < PhysicsManager >( );
	}

	public static PhysicsManager PhysicsMan
	{
		get { return mPhysicsManager; }	
	}
	
	public static GameManager Game
	{
		get { return mGameManager; }	
	}

}
