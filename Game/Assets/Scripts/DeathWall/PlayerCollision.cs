﻿//====================================================================
// Player Collision Class
// Description:		Detects when the Death Wall hits the player
// Created By: 		Greig Barker
// Date: 			12/03/14
// Edits:			
//
// Acknowledgements:					 
//====================================================================

using UnityEngine;
using System.Collections;

[RequireComponent( typeof ( BoxCollider2D ) ) ]

public class PlayerCollision : MonoBehaviour 
{
	[SerializeField] private int mNumberOfRays = 4;
	[SerializeField] private LayerMask mLayerMask = 0;
	private BoxCollider2D mCollider;
	private Vector3 mCentre;
	private Vector3 mSize;
	private bool mHitPlayer = false;
	private const float mSPACING = 0.0001f; //stop player overlapping the platfroms
	//GameObject mPlayer;
	
	// Use this for initialization
	void Start () 
	{
		//Gets the attached collider along and store size
		mCollider = GetComponent <BoxCollider2D> ();
		mCentre = mCollider.center;
		mSize = mCollider.size;	
		//mPlayer = GameObject.FindWithTag ("Player"); //Fills the Player object 
	}
	
	public void CollisionDetection(Vector2 _speed)
	{//Detects collisions with the wall nodes
		
		CheckPlayerHit( ref _speed);
		//return _speed;
	}

	public bool HitPlayer()
	{
		return mHitPlayer;
	}

	void PlayerCollidedWith(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Player")
		{
			coll.gameObject.SendMessage("ResetPlayer", null);
		}
	}

	private void CheckPlayerHit( ref Vector2 _speed) 
	{//Checks for collisons in the vertical axis
		
		Vector3 position = transform.position; //local position
		RaycastHit2D hit;
		
		for ( int rays = 0; rays <= mNumberOfRays; rays++ ) 
		{	
			//casts rays from the bottom left, centre and right of the collider
			Vector3 origin;
			float direction = Mathf.Sign( _speed.x ); //Get the sign of the vertical speed
			//casts rays from the side top, centre and bottom of the collider
			origin.x = position.x  + mCentre.x + ( mSPACING * direction ) + mSize.x / 2  * direction;
			origin.y = ( position.y + mCentre.y -  mSize.y / 2  ) + ( ( mSize.y / mNumberOfRays ) * rays ) ;
			origin.z = position.z;
			//cast a ray in the direction of the horizontal speed
			hit = Physics2D.Raycast( origin, new Vector2 ( direction, 0 ),  Mathf.Abs( _speed.x ), mLayerMask );
			
			//Check if the raycast has hit the Player
			if( hit.collider != null )
			{
				mHitPlayer = true;
				Debug.Log("Hit Player");


				break;				
			}
			else{
				mHitPlayer = false;
				Debug.DrawRay( new Vector3( origin.x, origin.y, 0.0f ),  new Vector3( direction, 0.0f, 0.0f ) );
			}
		}
	}	
}

