﻿//====================================================================
// DeathWall Class
// Description:		Creates the death wall that the player has to avoid while completing the level
// Created By: 		Greig Barker
// Date: 			04/03/12
// Edits:
// Acknowledgements:
//====================================================================

using UnityEngine;
using System.Collections;

//Adds required functions
[RequireComponent( typeof ( NodeCollisions ) ) ]
[RequireComponent( typeof ( PlayerCollision ) ) ]

public class DeathWall : MonoBehaviour 
{
	
	[SerializeField] private Vector2 mSpeed;
	private NodeCollisions mCollisions;
	private PlayerCollision mPlayerCollision;
	GameObject PlayerObject;
	
	// Use this for initialization
	void Start () 
	{
		mSpeed.x = 0.02f;
		mSpeed.y = 0.0f;
		mPlayerCollision = GetComponent <PlayerCollision> ();
		PlayerObject = GameObject.FindWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!Managers.Game.GetPaused())
		{
			mCollisions = GetComponent <NodeCollisions>();
			mSpeed = mCollisions.CollisionDetection (mSpeed);
			mPlayerCollision.CollisionDetection (mSpeed);
			//Debug.Log ("Death Wall Speed: " + mSpeed.x);
			
			transform.Translate(new Vector3( mSpeed.x, mSpeed.y, 0 ) );

			if (mPlayerCollision.HitPlayer ()) 
			{
				Reset ();
				Application.LoadLevel(Application.loadedLevel);
			}
			if (mSpeed.x == 0)
			{
				Reset ();
			}
		}
	}
	
	//Get Death walls current speed
	Vector2 GetSpeed()
	{
		return mSpeed;
	}
	
	//Set Death walls speed 
	void SetSpeed(ref Vector2 speed)
	{
		if (mSpeed != speed)
		{
			mSpeed = speed;
		}
	}

	void Reset()
	{
		transform.position = new Vector3(-10, 0, 0);
		mSpeed.x = 0.02f;
	}
}
