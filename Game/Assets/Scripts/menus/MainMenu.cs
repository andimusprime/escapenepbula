﻿//====================================================================
// Main Menu Class
// Description:	Contains all the functionality for the main menu
// Functions:  Start() : Sets all the hover booleans to false and creates the buttons
//			   CheckMouseOver(): Checks if any of the GUI buttons contain the mouse. 
// 								 If they do, the texture is swapped to give the appearence of being highlighted.
//			   OnGUI(): Has methods designed to move from the menu to any of the other menu scenes on GUI button press.
// Created By: Tom Brown
// Date: 10/03/2014
// Edits: Added functionality to keep all the buttons proportional to the screen size
//		  Added Funcioanlity to have the buttons light up on mouseover.
//		  Removed the bug where multiple buttons light up when hovering over individual buttons
// Acknowledgements: Digital tutors course on how to create GUI buttons for menus.
//====================================================================

using UnityEngine;
using System.Collections;
[ExecuteInEditMode]

public class MainMenu : MonoBehaviour 
{
	//Member variables
	public GUISkin mMenuGUISkin;
	public _GUIClasses.Location center = new _GUIClasses.Location();
	//Texture variables
	//The need for 3 texture variables is to have one "empty" texture varible
	//to store which is the current texture being used. The other two are the highlighted and
	//non highlighted textures.
	public Texture2D mNewGame;
	public Texture2D mNewGameSelected;
	public Texture2D mNewGameOff;
	public Texture2D mLevelSelect;
	public Texture2D mLevelSelectSelected;
	public Texture2D mLevelSelectOff;
	public Texture2D mCredits;
	public Texture2D mCreditsSelected;
	public Texture2D mCreditsOff;
	public Texture2D mControls;
	public Texture2D mControlsSelected;
	public Texture2D mControlsOff;
	private Vector2 mMousePos;
	//Button GUI button variables
	private Rect mNewButton;
	private Rect mLevelButton;
	private Rect mCreditsButton;
	private Rect mControlsButton;
	//Mouse over checking variables
	public bool mHoverNewGame;
	public bool mHoverLevelSelect;
	public bool mHoverCredits;
	public bool mHoverControls;

	void Start()
	{
		//Set mouse over booleans to false to assign unhighl ighted textures to the buttons
		mHoverNewGame = false;
		mHoverLevelSelect = false;
		mHoverCredits = false;
		mHoverControls = false;
		//Create the buttons to appear on screen on start up.
		//The magic numbers in order represent:
		// 1. the percentage scale factor of the screen width in the x direction when calculating the x coordinate of the GUI button
		// 2. the percentage scale factor of the screen height in the x direction when calculating the y coordinate of the GUI button
		// 3. the ratio of the width of the button compared with the texture width in oder to not make the texture appear too small or large on screen
		// 4. the ration of the height of the button compared with the texture height in order not to make the texture appear to small or large on screen
		// these numbers allow the GUI buttons to scale according to the screen resolution and have position based upon the height and width of the screen 
		// and arent based on pixel coordinates.
		mNewButton = new Rect (Screen.width * 0.29f,Screen.height * 0.85f, Screen.width * (30.0f/mNewGame.width), Screen.height * (3.0f/mNewGame.height));
		mLevelButton = new Rect (Screen.width * 0.44f,Screen.height * 0.85f, Screen.width * (30.0f/mLevelSelect.width), Screen.height * (3.0f/mLevelSelect.height));
		mControlsButton = new Rect (Screen.width * 0.62f,Screen.height * 0.85f, Screen.width * (30.0f/mControls.width) , Screen.height * (3.0f/mControls.height));
		mCreditsButton = new Rect (Screen.width * 0.80f,Screen.height * 0.85f, Screen.width * (30.0f/mCredits.width) , Screen.height * (3.0f/mCredits.height));
	}

	// Update is called once per frame
	void Update() 
	{
		center.updateLocation();
		CheckMouseOver();
	
	}

	void CheckMouseOver()
	{
		mMousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		//Checks if mouse is hovering over each button in the scene, if it is assign the associated 
		// boolean to true. 
		if(mNewButton.Contains(mMousePos))
		{
			mHoverNewGame = true;
			mHoverLevelSelect = false;
			mHoverControls = false;
			mHoverCredits = false;
		}
		else if(mLevelButton.Contains(mMousePos))
		{
			mHoverLevelSelect = true;
			mHoverNewGame = false;
			mHoverControls = false;
			mHoverCredits = false;
		}
		else if(mControlsButton.Contains(mMousePos))
		{
			mHoverControls = true;
			mHoverNewGame = false;
			mHoverLevelSelect = false;
			mHoverCredits = false;
		}
		else if(mCreditsButton.Contains(mMousePos))
		{
			mHoverCredits = true;
			mHoverNewGame = false;
			mHoverLevelSelect = false;
			mHoverControls = false;
		}
		else
		{
			mHoverNewGame = false;
			mHoverLevelSelect = false;
			mHoverControls = false;
			mHoverCredits = false;
		}

		//Assigns the correspondging texture to each button dependin upon
		//the logic state of the boolean.
		if(mHoverNewGame)
		{
			mNewGame = mNewGameSelected;
		}
		else if(mHoverNewGame == false)
		{
			mNewGame = mNewGameOff;
		}
		if(mHoverLevelSelect)
		{
			mLevelSelect = mLevelSelectSelected;
		}
		else if(mHoverLevelSelect == false)
		{
			mLevelSelect = mLevelSelectOff;
		}
		if(mHoverControls)
		{
			mControls = mControlsSelected;
		}
		else if(mHoverControls == false)
		{
			mControls = mControlsOff;
		}
		if(mHoverCredits)
		{
			mCredits = mCreditsSelected;
		}
		else if(mHoverCredits == false)
		{
			mCredits = mCreditsOff;
		}
	}


	void OnGUI()
	{
		//the Gui depth is -1 to make sure the background is behind everything in the scene
		GUI.skin = mMenuGUISkin;
		GUI.depth = -1;
		//Loads the relevent scene depending upon which button is pressed
		//The magic numbers are explained above in the Start() they are exactly the same here.
		if (GUI.Button (new Rect (Screen.width * 0.29f,Screen.height * 0.85f, Screen.width * (30.0f/mNewGame.width), Screen.height * (3.0f/mNewGame.height)), mNewGame)) 
		{
			Application.LoadLevel("SRLevel1");	
		}
		if (GUI.Button (new Rect (Screen.width * 0.46f,Screen.height * 0.85f, Screen.width * (30.0f/ mLevelSelect.width), Screen.height * (3.0f/mLevelSelect.height)), mLevelSelect)) 
		{
			Application.LoadLevel("LevelSelect");	
		}
		if (GUI.Button (new Rect (Screen.width * 0.63f,Screen.height * 0.85f, Screen.width * (30.0f/mControls.width) , Screen.height * (3.0f/mControls.height)), mControls)) 
		{
			Application.LoadLevel("Controls");	
		}
		if (GUI.Button (new Rect (Screen.width* 0.80f,Screen.height*0.85f, Screen.width * (30.0f/mCredits.width) , Screen.height * (3.0f/mCredits.height)), mCredits)) 
		{
			Application.LoadLevel("Credits");
		}

	}
}
