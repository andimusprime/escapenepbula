﻿//====================================================================
// Level Select Class
// Description:	Contains all the functionality for the level select menu
// Functions:  Start() : Sets the hover boolean to false and creates the button
//			   CheckMouseOver(): Checks if the GUI button contains the mouse. 
// 								 If it does, the texture is swapped to give the appearence of being highlighted.
//			   OnGUI(): Has the method implimented of moving back to the main menu from this scene.
// Created By: Tom Brown
// Date: 10/03/2014
// Edits: Added functionality to keep all the buttons proportional to the screen size
//		  Added Funcioanlity to have the buttons light up on mouseover.
// Acknowledgements: Digital tutors course on how to create GUI buttons for menus.
//====================================================================

using UnityEngine;
using System.Collections;
[ExecuteInEditMode]

public class LevelSelect : MonoBehaviour 
{
	//memeber variables
	public GUISkin mLevelSelectGUISkin;
	public _GUIClasses.Location center = new _GUIClasses.Location();
	//texture variables
	//The need for 3 texture variables is to have one "empty" texture varible
	//to store which is the current texture being used. The other two are the highlighted and
	//non highlighted textures.
	public Texture2D mBackButton;
	public Texture2D mBackButtonSelected;
	public Texture2D mBackButtonOff;
	public Texture2D mLevelOneButton;
	public Texture2D mLevelTwoButton;
	public Texture2D mLevelThreeButton;
	//mouse over checking variables
	public bool mHover;
	private Vector2 mMousePos;
	//Button Gui varialbes.
	private Rect mButton;

	void Start()
	{
		mHover = false;
		//Create the button to appear on screen on start up.
		//The magic numbers in order represent:
		// 1. the percentage scale factor of the screen width in the x direction when calculating the x coordinate of the GUI button
		// 2. the percentage scale factor of the screen height in the x direction when calculating the y coordinate of the GUI button
		// 3. the ratio of the width of the button compared with the texture width in oder to not make the texture appear too small or large on screen
		// 4. the ration of the height of the button compared with the texture height in order not to make the texture appear to small or large on screen
		// these numbers allow the GUI buttons to scale according to the screen resolution and have position based upon the height and width of the screen 
		// and arent based on pixel coordinates.
		mButton = new Rect(Screen.width * 0.80f,Screen.height * 0.85f,Screen.width * (30.0f/mBackButton.width), Screen.height * (3.0f/mBackButton.height));
	}


	// Update is called once per frame
	void Update() 
	{
		center.updateLocation();
		CheckMouseOver ();
	}


	void CheckMouseOver()
	{
		mMousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		//Checks if mouse is hovering over the back button and if it is assings the back boolean to true
		if(mButton.Contains(mMousePos))
		{
			mHover = true;
		}
		else
		{
			mHover = false;
		}

		//checks the state of the boolean and assigns the corresponding texture
		if(mHover)
		{
			mBackButton = mBackButtonSelected;
		}
		else if(mHover == false)
		{
			mBackButton = mBackButtonOff;
		}
	}



	void OnGUI()
	{
		//the Gui depth is -1 to make sure the background is behind everything in the scene
		GUI.skin = mLevelSelectGUISkin;
		GUI.depth = -1;
		//Loads the relevent scene depending upon which button is pressed
		//The magic numbers are explained above in the Start() they are exactly the same here.
		//The level buttons themselves have no highlight aspect but the magic numbers apply in exactly the same way.
		if (GUI.Button (new Rect (Screen.width*0.80f,Screen.height*0.85f, Screen.width * (30.0f/mBackButton.width) , Screen.height * (3.0f /mBackButton.height)), mBackButton)) 
		{
			Application.LoadLevel("MainMenu");	
		}
		if (GUI.Button (new Rect (Screen.width*0.20f,Screen.height*0.45f, Screen.width * (60.0f/mLevelOneButton.width) , Screen.height * (30.0f/mLevelOneButton.height)), mLevelOneButton)) 
		{
			Application.LoadLevel("SRLevel1");	
			Managers.Game.SetLevel(GameManager.Level.ONE);
		}
		if (GUI.Button (new Rect (Screen.width*0.45f,Screen.height*0.45f, Screen.width * (60.0f/mLevelTwoButton.width) , Screen.height * (30.0f/mLevelTwoButton.height)), mLevelTwoButton)) 
		{
			Application.LoadLevel("SRLevel2");	
			Managers.Game.SetLevel(GameManager.Level.TWO);
		}
		if (GUI.Button (new Rect (Screen.width*0.70f,Screen.height*0.45f, Screen.width * (60.0f/mLevelThreeButton.width) , Screen.height * (30.0f/mLevelThreeButton.height)), mLevelThreeButton)) 
		{
			Application.LoadLevel("SRLevel3");	
			Managers.Game.SetLevel(GameManager.Level.THREE);
		}
	}
}