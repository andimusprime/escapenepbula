﻿//====================================================================
// Credits Class
// Description:	Contains all the functionality for the credits menu
// Functions:  Start() : Sets the hover boolean to false and creates the button
//			   CheckMouseOver(): Checks if the GUI button contains the mouse. 
// 								 If it does, the texture is swapped to give the appearence of being highlighted.
//			   OnGUI(): Has the method implimented of moving back to the main menu from this scene.
// Created By: Tom Brown
// Date: 10/03/2014
// Edits: Added functionality to keep all the buttons proportional to the screen size
//		  Added Funcioanlity to have the buttons light up on mouseover.
// Acknowledgements: Digital tutors course on how to create GUI buttons for menus.
//====================================================================

using UnityEngine;
using System.Collections;
[ExecuteInEditMode]

public class Credits : MonoBehaviour 
{
	//Member Variables
	public GUISkin mCreditsGUISkin;
	//Texture Variables
	//The need for 3 texture variables is to have one "empty" texture varible
	//to store which is the current texture being used. The other two are the highlighted and
	//non highlighted textures.
	public Texture2D mBackButton;
	public Texture2D mBackSelected;
	public Texture2D mBackoff;
	//mouse over checking variables
	public bool mHover;
	Vector2 mMousePos;
	//Button Gui variable
	Rect mButton;
	
	void Start()
	{
		//Create the button to appear on screen on start up.
		//The magic numbers in order represent:
		// 1. the percentage scale factor of the screen width in the x direction when calculating the x coordinate of the GUI button
		// 2. the percentage scale factor of the screen height in the x direction when calculating the y coordinate of the GUI button
		// 3. the ratio of the width of the button compared with the texture width in oder to not make the texture appear too small or large on screen
		// 4. the ration of the height of the button compared with the texture height in order not to make the texture appear to small or large on screen
		// these numbers allow the GUI buttons to scale according to the screen resolution and have position based upon the height and width of the screen 
		// and arent based on pixel coordinates.
		mHover = false;
		mButton = new Rect(Screen.width * 0.80f,Screen.height * 0.85f,Screen.width * (30.0f/mBackButton.width), Screen.height * (3.0f/mBackButton.height));
	}
	// Update is called once per frame
	void Update () 
	{
		CheckMouseOver();
	}
	
	void CheckMouseOver()
	{
		mMousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		//Checks if mouse is hovering over the back button and if it is assings the back boolean to true
		if(mButton.Contains(mMousePos))
		{
			mHover = true;
		}
		else
		{
			mHover = false;
		}
		
		if(mHover)
		{
			mBackButton = mBackSelected;
		}
		else if(mHover == false)
		{
			mBackButton = mBackoff;
		}
	}
	
	
	
	void OnGUI()
	{
		//the Gui depth is -1 to make sure the background is behind everything in the scene
		GUI.skin = mCreditsGUISkin;
		GUI.depth = -1;
		//Loads the relevent scene depending upon which button is pressed
		//The magic numbers are explained above in the Start() they are exactly the same here.
		if(GUI.Button(new Rect(new Rect(Screen.width * 0.80f,Screen.height * 0.85f,Screen.width * (30.0f/mBackButton.width), Screen.height * (3.0f/mBackButton.height))), mBackButton))
		{
			
			Application.LoadLevel("MainMenu");	
		}
		
	}
}
