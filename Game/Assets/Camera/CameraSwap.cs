﻿using UnityEngine;
using System.Collections;

public class CameraSwap : MonoBehaviour {
	public Camera mMainCamera;
	public Camera mGameCamera;


	// Use this for initialization
	void Start () 
	{
		mMainCamera.enabled = true;
		mGameCamera.enabled = false;
	}

	
	// Update is called once per frame
	void Update () 
	{
		CameraUpdate();
	}
	
	void CameraUpdate()
	{
		//test to see if camera switch works
		if(Input.anyKeyDown)
		{
			mMainCamera.enabled = !mMainCamera.enabled;
			mGameCamera.enabled = !mGameCamera.enabled; 
		}
	}

}
